class Student:
    #Constructor
    
    def __init__(self, rn=1, n='x'):
        self.rollno = rn;
        self.name = n
    #take input from user and print it

    def getData(self):
        self.rollno = int(input('Enter a roll number of the student:'))
        self.name = input('Enter name of the student:')

    def showData(self):    
        print('The roll number of student is ={}'.format(self.rollno))
        print('The name of student is ={}'.format(self.name))

#object:
        
a1 = Student();
a1.showData();
a2 = Student();
a2.getData();
a2.showData()
#a1.getData()


