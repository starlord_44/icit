class Rectangle:
    #constuctor
    def __init__(self, l=1, w=1):
        self.length = l;
        self.width = w;
       
      
    def getData(self):
        self.length = float(input('Enter a length of rectangle:'))
        self.width = float(input('Enter a width of rectangle:'))
     

    def area(self):
        x = self.length * self.width
        y = 2 * (self.length + self.width)
        
        print('Length ={} , Width = {} '.format(self.length ,self.width ))
        print('Area of rectangle ={} '.format(self.length*self.width))
        print('Perimeter of rectangle ={}'.format(2 * (self.length + self.width)))
        
        
#Objects
a1=Rectangle();
a2=Rectangle(5,6)
a1.getData()
#a2.getData()
a1.area()
a2.area()
