class Animal:
    def eat(self):
        print('I can eat')


class Fish(Animal):
    def swim(self):
        print('I can swim')

class Man(Fish):
    def walk(self):
        print('I can walk')
    def sing(self):
        print('I can sing')
    def speak(self):
        print('I can speak')
    def dance(self):
        print('I can dance')
    def anything(self):
        print('I can do anything')


a1 = Animal();
a1.eat()

f1 = Fish();
f1.eat();
f1.swim();

m1 = Man();
m1.eat();
m1.swim();
m1.walk();
m1.sing();
m1.speak();
m1.dance();
m1.anything()
